package main

import "fmt"

func main() {

	ParseCLIFlags()
	cliHeader()
	if helpFlag {
		cliHelp()
		return
	}

	if !importFlag.set {
		fmt.Println("Import path has not been defined [-import ./path]")
		//importFlag.Set("./test")
		return
	}

	if !exportFile.set {
		fmt.Println("Export path has not been defined [-export ./path]")
		//importFlag.Set("./test")
		return
	}

	if !exportFile.set {
		fmt.Println("No output file has been specified [-o output.go]")
		//exportFile.Set("output.go")
		return
	}

	cliexport(importFlag.String(), exportFile.String())
}
