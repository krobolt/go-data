package example

import (
	"html/template"
	"log"
	"net/http"

	fs "gitlab.com/krobolt/go-data/fs"
)

var Assets *fs.DataStore

func Server() {
	handler := http.NewServeMux()
	handler.Handle("/whatever", fooHandler())
	http.ListenAndServe(":80", handler)
}

func fooHandler() http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {

		//fetch contents
		f, err := Assets.GetContents("path/name.ext")

		if err != nil {
			log.Fatal(err)
		}
		tmp, err := template.New("template").Parse(string(f.Bytes))
		if err != nil {
			log.Fatal(err)
		}
		tmp.Execute(w, nil)
	}
	return http.HandlerFunc(fn)
}
