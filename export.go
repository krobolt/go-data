package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"

	fs "gitlab.com/krobolt/go-data/fs"
)

func export(goout, export string, d *fs.DataStore) error {

	tmpdir, _ := ioutil.TempDir("tmpt", "*")
	defer os.RemoveAll(tmpdir)

	os.Remove("user.key")

	err := os.Mkdir(export, 0755)
	if err != nil {
		return err
	}

	key, err := os.Create("user.key")
	if err != nil {
		return err
	}
	defer key.Close()

	key.Write([]byte(d.Enc.GetKey()))
	if err != nil {
		return err
	}

	for _, dir := range d.Root {

		for _, content := range dir.Contents {
			if content.IsDir() {
				continue
			}

			filename := content.Path

			f, err := os.Open(filename)
			if err != nil {
				return err
			}

			content.ContentType, err = getType(f)
			if err != nil {
				return err
			}

			tmpout, err := ioutil.TempFile(tmpdir, "*")
			if err != nil {
				return fmt.Errorf("unable to create temp file error; %s", err.Error())
			}

			err = fs.Zip(filename, tmpout, f)
			if err != nil {
				return err
			}

			f.Close()
			tmpout.Seek(0, 0)

			tmp, err := ioutil.TempFile(export, "*.enc")
			if err != nil {
				return err
			}

			content.Path = tmp.Name()

			err = d.Enc.Encode(tmpout, tmp)
			if err != nil {
				return err
			}

			tmp.Close()
			os.Remove(tmpout.Name())

		}
	}

	//create the go file from template
	f, err := os.Create(goout)

	if err != nil {
		return err
	}
	defer f.Close()

	dirs, files, err := collectfiles(d, f)
	if err != nil {
		return err
	}
	writeGoFile(d, f, dirs, files)

	return nil
}

func writeGoFile(d *fs.DataStore, f *os.File, roots []string, contents []string) {

	io.Copy(f, bytes.NewReader([]byte(templateHeader)))

	rootpath := fmt.Sprintf(templateDirRoot, d.Path)
	io.Copy(f, bytes.NewReader([]byte(rootpath)))

	copyContents(f, roots)
	copyContents(f, contents)
	io.Copy(f, bytes.NewReader([]byte(templateEnd)))
}

func collectfiles(d *fs.DataStore, f *os.File) ([]string, []string, error) {
	dirs := make([]string, 0)
	files := make([]string, 0)

	for k, dir := range d.Root {

		o := fmt.Sprintf(templateDir, k, dir.Name, dir.Path)
		dirs = append(dirs, o)

		for name, file := range dir.Contents {
			o, err := templateFileFromInfo(name, file)
			if err != nil {
				return dirs, files, err
			}
			files = append(files, o)
		}
	}
	return dirs, files, nil
}

func templateFileFromInfo(name string, file *fs.FileInfo) (string, error) {

	bytes := file.Bytes
	content := file.ContentType
	size := file.Size()

	if file.IsDir() {
		content = ""
		size = 0
	}

	return fmt.Sprintf(
		templateContents,

		file.Name(),
		size,
		file.Mode(),
		file.IsDir(),
		file.ModTime().Unix(),
		file.Path,
		bytes,
		content,
		file.Parent,
		file.Parent,
		name,
	), nil
}

func copyContents(f *os.File, contents []string) {
	for _, c := range contents {
		io.Copy(f, bytes.NewReader([]byte(c)))
	}
}

//getType reads file header and returns formated content type
func getType(f *os.File) (string, error) {
	buffer := make([]byte, 512)
	f.Seek(0, 0)
	_, err := f.Read(buffer)
	if err != nil {
		panic(err)
	}
	f.Seek(0, 0)
	return http.DetectContentType(buffer), nil
}
