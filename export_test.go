package main

import (
	"io/ioutil"
	"os"
	"testing"
)

type testData struct {
	input    []string
	expected string
}

func Test_export(t *testing.T) {

	data := []testData{
		{
			input: []string{
				"a", "b", "c",
			},
			expected: "abc",
		},
		{
			input: []string{
				"12", "100", "14",
			},
			expected: "1210014",
		},
	}

	for _, test := range data {

		f, err := ioutil.TempFile("", "*")
		if err != nil {
			panic(err)
		}
		defer os.Remove(f.Name())

		copyContents(f, test.input)
		f.Close()

		actual, _ := ioutil.ReadFile(f.Name())
		expected := []byte(test.expected)

		if string(actual) != string(expected) {
			t.Errorf("strings do not match %s %s", string(actual), string(expected))
		}
	}

}
